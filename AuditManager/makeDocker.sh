#
# http://www.ronaldcirka.com/blog/2014/12/06/deploying-play-framework-to-elastic-beanstalk-in-docker/
#
# https://groups.google.com/forum/#!searchin/play-framework/elasticbeanstalk/play-framework/9ZYY-MQBq9k/pqzd6zhv1rsJ
#

# version="1.1D"

echo ""
echo "Get version number from build.sbt"

echo "Enter version number (eg 1.1D)?"
read version

echo "Building version $version"

#sbt clean docker:stage
activator clean docker:stage

cp Dockerrun.aws.json target/docker/stage

cd target/docker/stage

echo "EXPOSE 9000" >> Dockerfile

zip -r ../docker-audit-manager-$version.zip *

echo "****************************************************"
echo "* Docker zip ready to be uploaded to Elastic Beanstalk"
echo "* file: target/docker/docker-audit-manager-$version.zip"