package models;

import java.util.List;


public class AuditRecordService {
    /**
     * Create an AuditRecord
     *
     * @param AuditRecord data
     *
     * @return AuditRecord
     */
    public static AuditRecord create(AuditRecord data) {
        return AuditRecordDAO.create(data);
    }


    /**
     * Find an AuditRecord by user
     *
     * @param String userId
     *
     * @return AuditRecord
     */
    public static List<AuditRecord> findByUser(String id) {

        return  AuditRecordDAO.findByUser(id);
    }


    /**
     * Get audit records by account
     * @param Integer accountId
     * @return List<AuditRecord>
     */
    public static List<AuditRecord> getByAccountId(Integer accountId) {

        return AuditRecordDAO.getByAccountId(accountId);
    }

}