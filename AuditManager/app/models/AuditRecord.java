package models;

import com.fasterxml.jackson.annotation.JsonFormat;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.Date;

@Entity
public class AuditRecord{
    public static String TABLE = AuditRecord.class.getSimpleName();

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer id;

    @Column(name = "audit_timestamp", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss zzz")
    public Date audit_timestamp;

    @Constraints.Required
    public Integer account_id;

    @Constraints.Required
    public String audit_detail;

    @Constraints.Required
    public String user;

    public AuditRecord() {}


    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        AuditRecord aux = (AuditRecord) obj;

        if (id != null && aux.id != null)
            return (id == aux.id);
        else
            return (account_id.intValue() == aux.account_id.intValue() && audit_timestamp.equals(aux.audit_timestamp) );
    }


    @Override
    public String toString(){
        return "account_id: "+this.account_id+" user: "+this.user+" audit_detail: "+this.audit_detail;
    }
}