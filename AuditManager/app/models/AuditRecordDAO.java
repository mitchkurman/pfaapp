package models;

import play.db.jpa.JPA;

import javax.persistence.Query;
import java.util.List;

public class AuditRecordDAO {
    /**
     * Create an Audit Record
     *
     * @param AuditRecord model
     * @return AuditRecord
     */
    public static AuditRecord create (AuditRecord model) {
        JPA.em().persist(model);
        // Flush and refresh for check
        JPA.em().flush();
        JPA.em().refresh(model);
        return model;
    }

    /**
     * Find an employee by id
     *
     * @param String id
     *
     * @return AuditRecord
     */
    public static List<AuditRecord> findByUser(String user) {
        Query query = JPA.em().createQuery("SELECT m FROM " + AuditRecord.TABLE + " m where user = :user ORDER BY audit_timestamp desc");
        query.setParameter("user", user);
        return query.getResultList();
    }

    /**
     * Get AuditRecords
     *
     * @return List<AuditRecord>
     */
    public static List<AuditRecord> getByAccountId(Integer id) {
        Query query = JPA.em().createQuery("SELECT m FROM " + AuditRecord.TABLE + " m where account_id = :id ORDER BY audit_timestamp desc");
        query.setParameter("id", id);
        return query.getResultList();
    }

    /**
     * Get the page of employees
     *
     * @param Integer page
     * @param Integer size
     *
     * @return List<Employee>
     */
    public static List<AuditRecord> paginate(Integer page, Integer size) {
        return (List<AuditRecord>) JPA.em().createQuery("SELECT * FROM " + AuditRecord.TABLE + " where acct_id = :id ORDER BY id").setFirstResult(page*size).setMaxResults(size).getResultList();
    }

    /**
     * Get the number of total row
     *
     * @return Long
     */
    public static Long count() {
        return (Long) JPA.em().createQuery("SELECT count(m) FROM " + AuditRecord.TABLE + " m").getSingleResult();
    }
}