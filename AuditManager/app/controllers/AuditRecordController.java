package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.AuditRecord;
import models.AuditRecordService;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;

import java.util.List;

public class AuditRecordController extends Controller {
    static Form<AuditRecord> auditRecordForm = Form.form(AuditRecord.class);

    /**
     * Add the content-type json to response
     *
     * @param Result httpResponse
     *
     * @return Result
     */
    public Result jsonResult(Result httpResponse) {
        response().setContentType("application/json; charset=utf-8");
        return httpResponse;
    }

    /**
     * Get the index page
     *
     * @return Result
     */
    public Result index() {
        return ok(index.render());
    }

    public Result ping() {
        return ok(System.currentTimeMillis()+"-ok");
    }

    /**
     * Get the audit records using account id as search key
     *
     * @param Integer accountId
     *
     * @return Result
     */
    @Transactional(readOnly = true)
    public Result listByAccountId(Integer accountId) {
        List models = AuditRecordService.getByAccountId(accountId);

        ObjectNode result = Json.newObject();
        result.put("data", Json.toJson(models));

        return jsonResult(ok(result));
    }

    /**
     * Get the audit records using user id as search key
     *
     * @param String userId
     *
     * @return Result
     */
    @Transactional(readOnly = true)
    public Result listByUserId(String userId) {
        List models = (List)AuditRecordService.findByUser(userId);

        ObjectNode result = Json.newObject();
        result.put("data", Json.toJson(models));

        return jsonResult(ok(result));
    }


    /**
     * Create an employee with the data of request
     *
     * @return Result
     */
    @Transactional
    public Result create() {
        Form<AuditRecord> auditRecordFormData = auditRecordForm.bindFromRequest();
        if (auditRecordFormData.hasErrors()) {
            return jsonResult(badRequest(auditRecordFormData.errorsAsJson()));
        }
        AuditRecord newAR = AuditRecordService.create(auditRecordFormData.get());
        return jsonResult(created(Json.toJson(newAR)));
    }


}
