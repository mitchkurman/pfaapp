-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Sep 14, 2016 at 03:27 AM
-- Server version: 5.5.38
-- PHP Version: 5.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `pfa-app`
--

-- --------------------------------------------------------

--
-- Table structure for table `AuditRecord`
--

CREATE TABLE `AuditRecord` (
`id` int(11) NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `audit_detail` varchar(255) DEFAULT NULL,
  `audit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `AuditRecord`
--


-- --------------------------------------------------------

--
-- Table structure for table `Employee`
--

CREATE TABLE `Employee` (
`employee_id` int(11) NOT NULL,
  `department_name` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `hire_date` datetime DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `phone_extension` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `added_by_user_id` varchar(255) DEFAULT NULL,
  `last_updated_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `Employee`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `AuditRecord`
--
ALTER TABLE `AuditRecord`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Employee`
--
ALTER TABLE `Employee`
 ADD PRIMARY KEY (`employee_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `AuditRecord`
--
ALTER TABLE `AuditRecord`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `Employee`
--
ALTER TABLE `Employee`
MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;