$(document).ready(function() {

    $('#updateFormSubmit').click(function(e) {
        e.preventDefault();
        var formArray = $('#updateForm').serializeArray();
        //alert(formArray);
            // send ajax
        $.ajax({

            url: 'http://pfaapp.mitchkurman.us:9000/employees', // url where to submit the request
            type: "PUT", // type of action POST || GET
            dataType: 'json', // data type
            data: $('#updateForm').serializeArray(), // post data || get data
            success: function (result) {
                console.log(result);
            },
            error: function (xhr, resp, text) {
                console.log(xhr, resp, text);
            }
        });
    });


});