package models;

import java.util.List;


public class EmployeeRecordService {
    /**
     * Create an AuditRecord
     *
     * @param Employee data
     *
     * @return AuditRecord
     */
    public static Employee create(Employee data) {

        return EmployeeDAO.create(data);
    }


    /**
     * Find an AuditRecord by user
     *
     * @param String userId
     *
     * @return List<Employee>
     */
    public static List<Employee> list() {

        return  EmployeeDAO.list();
    }


    /**
     * Get audit records by account
     * @param Integer employeeId
     * @return List<AuditRecord>
     */
    public static Employee getByEmployeeId(Integer employeeId) {

        return EmployeeDAO.getByEmployeeId(employeeId);
    }

    /**
     * Get audit records by account
     * @param Integer employeeId
     * @return List<AuditRecord>
     */
    public static Employee update(Employee data) {

        return EmployeeDAO.update(data);
    }

}