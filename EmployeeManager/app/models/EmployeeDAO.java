package models;

import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.Query;
import java.util.List;

public class EmployeeDAO {
    /**
     * Create an Audit Record
     *
     * @param Employee model
     * @return Employee
     */
    public static Employee create (Employee model) {
        JPA.em().persist(model);
        // Flush and refresh for check
        JPA.em().flush();
        JPA.em().refresh(model);
        return model;
    }

    @Transactional
    public static Employee update (Employee model) {

        Employee emp = JPA.em().find(Employee.class,model.employee_id);
        play.Logger.info(emp.toString());
//        JPA.em().getTransaction().begin();
        emp.firstname = model.firstname;
        emp.lastname = model.lastname;
        emp.department_name = model.department_name;
        emp.last_updated_user_id = model.last_updated_user_id;
        emp.phone_extension = model.phone_extension;
        emp.title = model.title;
//        JPA.em().getTransaction().commit();
        // Flush and refresh for check
        JPA.em().flush();
        JPA.em().merge(model);
        return model;
    }

    /**
     * Find an employee by id
     *
     * @param String id
     *
     * @return List<Employee>
     */
    public static List<Employee> list() {
        Query query = JPA.em().createQuery("SELECT m FROM " + Employee.TABLE + " m ORDER BY hire_date desc");
        return query.getResultList();
    }

    /**
     * @param Integer id
     *
     * @return List<AuditRecord>
     */
    public static Employee getByEmployeeId(Integer id) {

        return JPA.em().find(Employee.class, id);
    }

    /**
     * Get the page of employees
     *
     * @param Integer page
     * @param Integer size
     *
     * @return List<Employee>
     */
    public static List<Employee> paginate(Integer page, Integer size) {
        return (List<Employee>) JPA.em().createQuery("SELECT * FROM " + Employee.TABLE + " where acct_id = :id ORDER BY id").setFirstResult(page*size).setMaxResults(size).getResultList();
    }

    /**
     * Get the number of total row
     *
     * @return Long
     */
    public static Long count() {
        return (Long) JPA.em().createQuery("SELECT count(m) FROM " + Employee.TABLE + " m").getSingleResult();
    }
}