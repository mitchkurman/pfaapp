package models;

import com.fasterxml.jackson.annotation.JsonFormat;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Employee{
    public static String TABLE = Employee.class.getSimpleName();

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer employee_id;

//    @Column(name = "hire_date", columnDefinition="DATE DEFAULT TODAY", insertable = true, updatable = true)
    @Constraints.Required
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    public Date hire_date;

    @Constraints.Required
    public String department_name;

    @Constraints.Required
    public String firstname;

    @Constraints.Required
    public String lastname;

    @Constraints.Required
    public String title;

    @Constraints.Required
    public String phone_extension;

    @Constraints.Required
    public String last_updated_user_id;

    public Employee() {}


    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Employee aux = (Employee) obj;

        if (employee_id != null && aux.employee_id != null)
            return (employee_id == aux.employee_id);
        else
            return false;
    }

    @Override
    public String toString(){
        return "employee_id: "+this.employee_id+" hire_date: "+this.hire_date+" department_name: "+this.department_name+
                " firstname: "+this.firstname+" lastname: "+this.lastname+" title: "+this.title+" phone_extension: "+phone_extension;
    }

}