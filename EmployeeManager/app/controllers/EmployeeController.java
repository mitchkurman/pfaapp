package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Employee;
import models.EmployeeRecordService;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.F.Promise;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;
import views.html.list;
import views.html.update;

import javax.inject.Inject;
import java.util.List;

public class EmployeeController extends Controller {
    static Form<Employee> employeeForm = Form.form(Employee.class);
    @Inject WSClient ws;

    /**
     * Add the content-type json to response
     *
     * @param Result httpResponse
     *
     * @return Result
     */
    public Result jsonResult(Result httpResponse) {
        response().setContentType("application/json; charset=utf-8");
        return httpResponse;
    }

    /**
     * Get the update page
     *
     * @return Result
     */
    public Result updateTesterPage() {
        return ok(update.render());
    }

    /**
     * Get the list page
     *
     * @return Result
     */
    public Result listTesterPage() {
        return ok(list.render());
    }


    /**
     * Get the index page
     *
     * @return Result
     */
    public Result index() {
        return ok(index.render());
    }


    /**
     * Get the audit records using account id as search key
     *
     * @return Result
     */
    @Transactional(readOnly = true)
    public Result list() {
        List models = EmployeeRecordService.list();

        ObjectNode result = Json.newObject();
        result.put("data", Json.toJson(models));

        return jsonResult(ok(result));
    }

    /**
     * Get the audit records using user id as search key
     *
     * @param String employeeId
     *
     * @return Result
     */
    @Transactional(readOnly = true)
    public Result getEmployee(Integer employeeId) {
        Employee models = EmployeeRecordService.getByEmployeeId(employeeId);

        ObjectNode result = Json.newObject();
        result.put("data", Json.toJson(models));

        return jsonResult(ok(result));
    }


    /**
     * Create an employee with the data of request
     *
     * @return Result
     */
    @Transactional
    public Result create() {
        Form<Employee> employeeFormData = employeeForm.bindFromRequest();
        play.Logger.info("Mitch Employee form record is:" + employeeFormData.toString());

        if (employeeFormData.hasErrors()) {
            return jsonResult(badRequest(employeeFormData.errorsAsJson()));
        }

        Employee newEE = EmployeeRecordService.create(employeeFormData.get());

        ObjectNode js = JsonNodeFactory.instance.objectNode();
        js.put("user",newEE.last_updated_user_id);
        js.put("account_id",newEE.employee_id);

        String auditString =  "hire_date: "+newEE.hire_date+" department_name: "+newEE.department_name+
                " firstname: "+newEE.firstname+" lastname: "+newEE.lastname+" title: "+newEE.title+" phone_extension: "+newEE.phone_extension;

        js.put("audit_detail","Added New Employee: "+auditString);



        Result res = jsonResult(created(Json.toJson(newEE)));

        String url = "http://localhost:9001/audit";
        Promise<JsonNode> jsonProm = ws.url(url).post(js).map(
                response -> {
                    return response.asJson();
                });

        int timeout = 1000;
        JsonNode out = jsonProm.get(timeout);

        System.out.println(out.toString());

//        ws.url("http://localhost:9001/audit").setContentType("application/json").post(js).map(response -> {
//            return response.asJson();
//        });
//        System.out.println(response);
        return jsonResult(ok(out));
    }

    /**
     * Create an employee with the data of request
     *
     * @return Result
     */
    @Transactional
    public Result update() {
    /*
    Data in form should be a JSON object
    */
        Form<Employee> employeeFormData = employeeForm.bindFromRequest();
        play.Logger.info("submitted json object in update request:" + employeeFormData.toString());

        if (employeeFormData.hasErrors()) {
            return jsonResult(badRequest(employeeFormData.errorsAsJson()));
        }

        Employee newEE = EmployeeRecordService.update(employeeFormData.get());

        ObjectNode js = JsonNodeFactory.instance.objectNode();
        js.put("user",newEE.last_updated_user_id);
        js.put("account_id",newEE.employee_id);

        String auditString =  "hire_date: "+newEE.hire_date+" department_name: "+newEE.department_name+
                " firstname: "+newEE.firstname+" lastname: "+newEE.lastname+" title: "+newEE.title+" phone_extension: "+newEE.phone_extension;

        js.put("audit_detail","Updated Employee: "+auditString);



        Result res = jsonResult(created(Json.toJson(newEE)));

        String url = "http://localhost:9001/audit";
        Promise<JsonNode> jsonProm = ws.url(url).post(js).map(
                response -> {
                    return response.asJson();
                });

        int timeout = 1000;
        JsonNode out = jsonProm.get(timeout);

        System.out.println(out.toString());

//        ws.url("http://localhost:9001/audit").setContentType("application/json").post(js).map(response -> {
//            return response.asJson();
//        });
//        System.out.println(response);
        return jsonResult(ok(out));
    }


}
