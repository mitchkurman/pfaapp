package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.add;
import views.html.audit;
import views.html.main;
import views.html.newEE;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * This controller demonstrates how to use dependency injection to
 * bind a component into a controller class. The class contains an
 * action that shows an incrementing count to users. The {@link Counter}
 * object is injected by the Guice dependency injection system.
 */
@Singleton
public class CreateEEController extends Controller {
    @Inject WebJarAssets webJarAssets;

    /**
     * An action that responds with the {@link Counter}'s current
     * count. The result is plain text. This action is mapped to
     * <code>GET</code> requests with a path of <code>/count</code>
     * requests by an entry in the <code>routes</code> config file.
     */
    public Result index() {
        return ok(newEE.render(webJarAssets));
    }

    public Result main() {
        return ok(main.render(webJarAssets));
    }

    public Result add() {
        return ok(add.render(webJarAssets));
    }

    public Result audit() {
        return ok(audit.render(webJarAssets));
    }
}
