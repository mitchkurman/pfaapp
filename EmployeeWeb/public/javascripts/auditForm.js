/**
 * Created by mitchellkurman on 9/11/16.
 */


$(document).ready(function() {
    /*
     if click on the radio button for account_id, hide user name entry field
     */
    $(function() {
        $("#ajaxReply").hide();
        $("#reset").hide();
    });

    $("#radio_account_id").click(function () {
        $("#input-user-id").hide();
        $("#user").val("");
        $("#input-acct-id").show();
    });
    /*
     if click on the radio button for user name, hide account_id entry field
     */
    $("#radio_user").click(function () {
        $("#input-user-id").show();
        $("#input-acct-id").hide();
        $("#account_id").val("");
    });
    /*
     submit action - make ajax call
     */
    $("#submit").click(function (e) {
        e.preventDefault();
        if ($("#user").val() != '') {
            usrNm = $("#user").val();
            // send ajax
            $.ajax({
                url: 'http://pfaapp.mitchkurman.us:9001/audit/users/' + usrNm, // url where to submit the request
                type: "GET", // type of action POST || GET
                dataType: 'json', // data type
                //data: $("#form").serialize(), // post data || get data
                success: function (result) {
                    $("#searchSection").hide();
                    $("#ajaxReply").show();
                    $("#reset").show();
                    // you can see the result from the console
                    // tab of the developer tools
                    //console.log(result.data);
                    $.each(result.data, function (index, item) {
                        addToTable(item);
                    });
                },
                error: function (xhr, resp, text) {
                    console.log(xhr, resp, text);
                }
            });
        } else {
            if ($("#account_id").val() != '') {
                acctId = $("#account_id").val();
                // send ajax
                $.ajax({
                    url: 'http://pfaapp.mitchkurman.us:9001/audit/accounts/' + acctId, // url where to submit the request
                    type: "GET", // type of action POST || GET
                    dataType: 'json', // data type
                    //data : $("#form").serialize(), // post data || get data
                    success: function (result) {
                        $("#searchSection").hide();
                        $("#ajaxReply").show();
                        $("#reset").show();
                        // you can see the result from the console
                        // tab of the developer tools
                        //console.log(result.data);
                        $.each(result.data, function (index, item) {
                            addToTable(item);
                        });
                    },
                    error: function (xhr, resp, text) {
                        console.log(xhr, resp, text);
                    }
                });
            }
        }
        return false;
    });

    function addToTable(item){
        var eachrow = "<tr>"
            + "<td class='span8'>" + item['audit_timestamp'] + "</td>"
            + "<td class='span8'>" + item['account_id'] + "</td>"
            + "<td class='span8'>" + item['user'] + "</td>"
            + "<td>" + item['audit_detail'] + "</td>"
            + "</tr>";
        console.log(eachrow);
        $("#auditTable").find('tbody').append($(eachrow)).show();
        //alert(row);
        //$("#tableRows").append(row);
    };

    $("#reset").click(function (e) {
        location.reload();
    });

});
