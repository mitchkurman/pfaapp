/**
 * Created by mitchellkurman on 9/11/16.
 */


$(document).ready(function() {
    //alert("ready");
    /*
     if click on the radio button for account_id, hide user name entry field
     */
    $(function() {
        $("#employeeList").hide();
        $("#employeeDetailForm").hide();
        //$("#reset").hide();
    });

    /*
     submit action - make ajax call
     */
    $("#listSubmit").click(function (e) {
        e.preventDefault();
        //alert('entered ajax');
            // send ajax
        $.ajax({
            url: 'http://pfaapp.mitchkurman.us:9000/employees', // url where to submit the request
            type: "GET", // type of action POST || GET
            dataType: 'json', // data type
            //data: $("#form").serialize(), // post data || get data
            success: function (result) {
                // you can see the result from the console
                // tab of the developer tools
                console.log(result.data);
                $.each(result.data, function (index, item) {
                    $("#employeeList").show();
                    addToTable(item);
                });
            },
            error: function (xhr, resp, text) {
                console.log(xhr, resp, text);
            }
        });
        return false;
    });

    /*
    When someone searches based on employee id, return the appropriate record
     */
    $("#searchSubmit").click(function (e) {
        e.preventDefault();
        //alert('entered ajax');
        var emplId = $("#searchEmployeeId").val();
        if (emplId != '') {
            // send ajax
            $.ajax({
                url: 'http://pfaapp.mitchkurman.us:9000/employees/' +emplId, // url where to submit the request
                type: "GET", // type of action POST || GET
                dataType: 'json', // data type
                //data: $("#form").serialize(), // post data || get data
                success: function (result) {
                    // you can see the result from the console
                    // tab of the developer tools
                    $("#employeeDetailForm").hide();
                    console.log(result.data);
                    var item = result.data;
                    $("#employeeDetailForm").show();
                    $("#employeeId").val(item['employee_id']);
                    $("#firstname").val(item['firstname']);
                    $("#lastname").val(item['lastname']);
                    $("#title").val(item['title']);
                    $("#department").val(item['department_name']);
                    $("#extension").val(item['phone_extension']);
                    $("#hiredate").val(item['hire_date']);
                    $("#lastupdated").val(item['last_updated_user_id']);
                },
                error: function (xhr, resp, text) {
                    console.log(xhr, resp, text);
                }
            });
        }
        return false;
    });


    /*
    Dynamically build the table with list results.  Table rows are clickable
     */
    function addToTable(item){
        var eachrow = "<tr id='"+item['employee_id']+"'>"
            + "<td>" + item['employee_id'] + "</td>"
            + "<td>" + item['lastname'] + "</td>"
            + "<td>" + item['firstname'] + "</td>"
            + "</tr>";
        console.log(eachrow);
        $("#employeeTable").find('tbody').append($(eachrow)).show();
        //alert(row);
        //$("#tableRows").append(row);
    };

    $("#reset").click(function (e) {
        location.reload();
    });

    /*
    When someone clicks on a table row to get more detail -
    the search will occur and the other fields will be hidden assuming a result is returned
     */
    $("tbody").delegate("tr","click", function(e) {
        //console.log($(this));
        //alert($(this)[0].id);
        e.preventDefault();
        //alert('entered ajax');
        var emplId = $(this)[0].id;
        if (emplId != '') {
            // send ajax
            $.ajax({
                url: 'http://pfaapp.mitchkurman.us:9000/employees/' +emplId, // url where to submit the request
                type: "GET", // type of action POST || GET
                dataType: 'json', // data type
                //data: $("#form").serialize(), // post data || get data
                success: function (result) {
                    // you can see the result from the console
                    // tab of the developer tools
                    $("#employeeDetailForm").hide();
                    console.log(result.data);
                    var item = result.data;
                    $("#employeeDetailForm").show();
                    $("#employeeId").val(item['employee_id']);
                    $("#firstname").val(item['firstname']);
                    $("#lastname").val(item['lastname']);
                    $("#title").val(item['title']);
                    $("#department").val(item['department_name']);
                    $("#extension").val(item['phone_extension']);
                    $("#hiredate").val(item['hire_date']);
                    $("#lastupdated").val(item['last_updated_user_id']);
                },
                error: function (xhr, resp, text) {
                    console.log(xhr, resp, text);
                }
            });
        }
        return false;
    });

    $('#updateFormSubmit').click(function(e) {
        e.preventDefault();
        //var formArray = $('#updateForm').serializeArray();
        //alert(formArray);

        /*
        Validate that there is a value in every field
         */
        var jsonObj;
        var validate = true;
        var fn = $("#firstname").val();
        var ln = $("#lastname").val();
        var t = $("#title").val();
        var dep = $("#department").val();
        var ext = $("#extension").val();
        var hd = $("#hiredate").val();
        var lu = $("#lastupdated").val();
        if(fn == '' || ln == '' || t == '' || dep == '' || ext == '' || hd == '' || lu == ''){
            validate = false;
        }else{
           jsonObj = {employee_id:$("#employeeId").val(),firstname:fn,lastname:ln,
               title:t,department_name:dep,phone_extension:ext,hire_date:hd,last_updated_user_id:lu};
            console.log("jsonObj: " +jsonObj);
        }

        // send ajax
        if(validate == true) {
            $.ajax({

                url: 'http://pfaapp.mitchkurman.us:9000/employees', // url where to submit the request
                type: "PUT", // type of action POST || GET
                dataType: 'json', // data type
                data: jsonObj, // post data || get data
                success: function (result) {
                    console.log(result);
                },
                error: function (xhr, resp, text) {
                    console.log(xhr, resp, text);
                }
            });
        }
    });

    $('#addFormSubmit').click(function(e) {
        e.preventDefault();
        //var formArray = $('#updateForm').serializeArray();
        //alert(formArray);

        /*
         Validate that there is a value in every field
         */
        var jsonObj;
        var validate = true;
        var fn = $("#firstname").val();
        var ln = $("#lastname").val();
        var t = $("#title").val();
        var dep = $("#department").val();
        var ext = $("#extension").val();
        var hd = $("#hiredate").val();
        var lu = $("#lastupdated").val();
        if(fn == '' || ln == '' || t == '' || dep == '' || ext == '' || hd == '' || lu == ''){
            validate = false;
        }else{
            jsonObj = {employee_id:$("#employeeId").val(),firstname:fn,lastname:ln,
                title:t,department_name:dep,phone_extension:ext,hire_date:hd,last_updated_user_id:lu};
            console.log("jsonObj: " +jsonObj);
        }

        // send ajax
        if(validate == true) {
            $.ajax({

                url: 'http://pfaapp.mitchkurman.us:9000/employees', // url where to submit the request
                type: "POST", // type of action POST || GET
                dataType: 'json', // data type
                data: jsonObj, // post data || get data
                success: function (result) {
                    console.log(result);
                    location.reload();
                },
                error: function (xhr, resp, text) {
                    console.log(xhr, resp, text);
                }
            });
        }else{
            //form validation error logic goes here
        }
    });
});
