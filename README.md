# PFAAPP - Project for Staff Architect Position

## Application Overview ##
The application is a multi-tier app constructed with the following technologies:  

* Data Tier: MySQL
* Web/App Tier (x2): Play Framework (Java)
* Portal (Data Mashup): Play Framework (Java, AdminLTE Template, JQuery, Bootstrap)

There were 2 API created for this project: 

* Employees (GET list, GET by employee, PUT, POST)
* Audit (GET by user, GET by account, POST)

Each API is hosted in it's own separate Web/App Tier - Employees is in EmployeeManager and Audit is in AuditManager.  The Portal application leverages the API for all its data requirements.  Employee API calls (PUT/POST) result in API call from Employee to Audit.

## Instructions on how to use:

**Homepage:** [http://pfaapp.mitchkurman.us:9002/main](http://pfaapp.mitchkurman.us:9002/main)

*All form fields are required - form validation has not been implemented*

## List/Edit Employee Records  
[http://pfaapp.mitchkurman.us:9002/main](http://pfaapp.mitchkurman.us:9002/main) brings up the application  
A couple of ways to interact with the 'manager'
  
 1. Click the submit button to list the employees  
 2. A table will present the list below. The table is clickable  
 3. Select a row and the detailed record will appear on lower right  
 4. Modify the data in the record and update *(note one of the other records in the list)*   
 5. In the box at the top right enter the employee id and submit  
 6. The box in the lower right will be updated with the detail for that id  
 7. Reset the page and perform in any order  

## Add New Employee Records   
 1. Navigate to the Add New Employees link  
 2. On that screen add new employees.    
*The user name field is intended to be the id of the person adding the record*  

## View Audit Records   
 1. Navigate to the View Audit Log screen. This page enables search for all the inserts/updates associated to an account (employee id)  
 2. The other option is to view all inserts/updates performed by a user name  
 3. Use Reset to start the search over using different criteria.  


## View Simple WebApps
* Employee Manager: [http://pfaapp.mitchkurman.us:9000/](http://pfaapp.mitchkurman.us:9000/)  
* Audit Manager: [http://pfaapp.mitchkurman.us:9001/](http://pfaapp.mitchkurman.us:9001/)  

## Page Load Time
Visible on the side nav of the portal.  Calculates in milliseconds time to load page using window.performance events.
More information available at [https://www.w3.org/TR/navigation-timing/timing-overview.png](https://www.w3.org/TR/navigation-timing/timing-overview.png).


-------------
Setup notes:

AuditManager run on port 9001
activator -Dhttp.port=9001 run
Base project was play framework
replies to GET request with JSON output
Queries local mysql db for data tier (JPA)


AuditWeb run on port 9004
activator -Dhttp.port=9004 run
http://localhost:9004/auditRecords
Brings up a 1 page app - 
used bootstrap and jquery
ajax call made from browser
Base project was play framework
Makes GET request


EmployeeManager run on port 9000
activator -Dhttp.port=9000 run
Base project was play framework
Implements POST, PUT, GET
Queries local mysql db for data tier (JPA)
When PUT/POST complete, makes POST to AuditManager 

EmployeeWeb run on port 9002
activator -Dhttp.port=9002 run
Base project was play framework
Portal uses Admin LTE template